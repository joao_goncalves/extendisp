

# extendisp

## A R package containing a collection of statistical tools to model extended dispersal kernels

### Summary   

1. Dispersal ecology is a topical discipline to understand fundamental ecological questions and to address urgent applied issues. Plant and animal populations rely on long distance dispersal (LDD) events to colonize distant suitable sites across managed, fragmented or degraded landscapes. Dispersal ecologists have collated a wealth dispersal distance records based on molecular markers, GPS-based telemetry devices, or fine-tuned models, but the estimation of the extent and frequency of LDD events beyond focal populations remains elusive. Developing new statistical tools or applying existing ones to take the most of readily available data sets is a timely task for dispersal ecologists.     

2. Here present the EXTENDISP R package that fits extreme value functions to recorded dispersal distances to infer and plot the frequency and extent of rare, and yet unobserved but not impossible, LDD events. EXTENDISP applies the block maxima and the threshold excess according to the sampling design and it accommodates ecological correlates, such as the type/or the size of the dispersal vector to improve the fitting of extreme value functions. 
     
3. Additional functionalities of EXTENDISP includes: (i) Identify a distance threshold as an objective and quantitative criteria to define LDD events; (ii) Compare and plot extended dispersal kernels that results from the activity of different dispersal vectors; (iii) Estimate the maximal dispersal distance or the probability of reaching distant locations, (iv) Perform statistical comparison of maximal dispersal distances among taxa or data sets; and (v) Quantify and plot changes in the dispersal ability of the study taxa, that might result, for example, from the loss or the gain of a dispersal vector (dispersal deficit/dispersal gain).
      
4. By providing extended dispersal kernels based on EVA, EXTENDISP is a suitable and timely tool to assist ecologists and practitioners in their study of the dispersal ability of a wide variety of taxa.





