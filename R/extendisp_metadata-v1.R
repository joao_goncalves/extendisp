


#' A example/test dataset on larval dispersal
#'
#' This datset includes larval dispersal distances. Includes the folowing variables:
#'
#' \itemize{
#'
#'    \item OffspringID 	
#'    \item Parent1_ID	
#'    \item Parent2	
#'    \item Distance_km	
#'    \item ParentRegion	
#'    \item DirectionNS	
#'    \item SettlerPLD	
#'    \item SettlerSL	
#'    \item Origin_sponge_length_cm	
#'    \item Origin_nr_sponge_tubes	
#'    \item Origin sponge depth_ft

#' }
#'
#' @name larvalDispersal
#'
#' @docType data
#'
#' @author Crístina Garcia \email{garciacristin@icloud.com}
#'
#' @references \url{????.com}
#'
#' @keywords data
#'
#' @usage data(larvalDispersal)
#'
#' @format A data frame with 120 rows and 11 column
#'
NULL

## ------------------------------------------------------------------------------------------ ##


#' A example/test dataset on pollen dispersal distance
#'
#' This datset includes pollen dispersal distances. Includes the folowing variables:
#'
#' \itemize{
#'
#'    \item DIST - dispersal distances (meters)
#' }
#'
#' @name pollenDist
#'
#' @docType data
#'
#' @author Crístina Garcia \email{garciacristin@icloud.com}
#'
#' @references \url{????.com}
#'
#' @keywords data
#'
#' @usage data(pollenDist)
#'
#' @format A data frame with 97 rows and 1 column
#'
NULL

## ------------------------------------------------------------------------------------------ ##

#' A example/test dataset on seed dispersal distance
#'
#' This datset includes seed dispersal distances. Includes the folowing variables:
#'
#' \itemize{
#'
#'    \item DIST - dispersal distances (meters)
#' }
#'
#' @name seedDist
#'
#' @docType data
#'
#' @author Crístina Garcia \email{garciacristin@icloud.com}
#'
#' @references \url{????.com}
#'
#' @keywords data
#'
#' @usage data(seedDist)
#'
#' @format A data frame with 430 rows and 1 column
#'
NULL

## ------------------------------------------------------------------------------------------ ##

#' A example/test dataset on seed shadow maximal distance
#'
#' This datset includes seed shadow maximal distance. Includes the folowing variables:
#'
#' \itemize{
#'
#'    \item N - observation index
#'    \item MAXDIST - maximum distance (meters)
#' }
#'
#' @name seedShadow1
#'
#' @docType data
#'
#' @author Crístina Garcia \email{garciacristin@icloud.com}
#'
#' @references \url{????.com}
#'
#' @keywords data
#'
#' @usage data(seedShadow1)
#'
#' @format A data frame with 500 rows and 1 column
#'
NULL

## ------------------------------------------------------------------------------------------ ##

#' A example/test dataset on seed shadow maximal distance
#'
#' This datset includes seed shadow maximal distance. Includes the folowing variables:
#'
#' \itemize{
#'
#'    \item N - observation index
#'    \item MAXDIST - maximum distance (meters)
#' }
#'
#' @name seedShadow1
#'
#' @docType data
#'
#' @author Crístina Garcia \email{garciacristin@icloud.com}
#'
#' @references \url{????.com}
#'
#' @keywords data
#'
#' @usage data(seedShadow2)
#'
#' @format A data frame with 500 rows and 1 column
#'
NULL


